package com.adamkorzeniak.ml.newman;

import com.adamkorzeniak.ml.newman.domain.Simulation;

public class App {

    public static void main(String[] args) {
        Simulation.sample().start();
    }
}
