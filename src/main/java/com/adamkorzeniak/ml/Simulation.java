package com.adamkorzeniak.ml;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import lombok.Getter;
import lombok.Setter;

public abstract class Simulation {

    public static final int DEFAULT_GENERATION_LIMIT = 1000;

    private final Environment environment;

    private final List<Double> overallFitness = new ArrayList<>();
    private final List<Double> bestFitness = new ArrayList<>();

    @Setter
    @Getter
    private boolean displayResultsAfterEachGeneration = false;

    @Getter
    private int currentGeneration = 1;

    public Simulation(Environment environment) {
        this.environment = environment;
    }

    public boolean hasNextGeneration() {
        return currentGeneration < DEFAULT_GENERATION_LIMIT;
    }

    public final Optional<Double> getOverallFitnessImprovement(int lastGenerations) {
        return getFitnessImprovement(lastGenerations, overallFitness);
    }

    public final Optional<Double> getBestFitnessImprovement(int lastGenerations) {
        return getFitnessImprovement(lastGenerations, bestFitness);
    }

    private Optional<Double> getFitnessImprovement(int lastGenerations, List<Double> overallFitnesses) {
        if (currentGeneration < lastGenerations) {
            return Optional.empty();
        }
        double currentFitness = overallFitnesses.get(currentGeneration);
        double previousFitness = overallFitnesses.get(currentGeneration - lastGenerations);
        return Optional.of(currentFitness / previousFitness);
    }

    public final void start() {
        while (hasNextGeneration()) {
            environment.generateNewGeneration();
            currentGeneration++;
            environment.runGeneration();
            storeGenerationSummary();
            if (displayResultsAfterEachGeneration) {
                environment.displayGeneration();
            }
        }
        environment.displayFinalResults();
    }

    private void storeGenerationSummary() {
        overallFitness.add(environment.getOverallFitness().orElse(null));
        bestFitness.add(environment.getBestFitness().orElse(null));
    }
}
